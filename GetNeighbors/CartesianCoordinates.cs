﻿using System;

namespace GetNeighbors
{
    public static class CartesianCoordinates
    {
        public static Point[] GetNeighbors(Point point, int h, params Point[] points)
        {
            if (points == null)
            {
                throw new ArgumentNullException(nameof(points));
            }

            if (h <= 0)
            {
                throw new ArgumentException("prikol");
            }

            int counter = 0;
            Point[] result = new Point[30];

            for (int i = 0; i < points.Length; i++)
            {
                if (Math.Abs(points[i].X - point.X) <= h && Math.Abs(points[i].Y - point.Y) <= h)
                {
                    result[counter] = points[i];
                    counter++;
                }
            }

            Point[] answer = new Point[counter];
            counter--;

            while (counter >= 0)
            {
                answer[counter] = result[counter];
                counter--;
            }

            return answer;
        }
    }
}
